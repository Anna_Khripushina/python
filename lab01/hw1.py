def func(x):
    """Проверка числа на кратность 3 и 5."""
    assert x > 0
    assert x == int(x)
    result = []
    if x % 3 == 0:
        result.append('foo')
    if x % 5 == 0:
        result.append('bar')
    return ' '.join(result) if result else str(x)

if __name__ == '__main__':
    assert func(15) == 'foo bar'
    assert func(25) == 'bar'
    assert func(24) == 'foo'
    assert func(17) == '17'